#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <iostream>

#define MAX_POZIOM 40
#define DLUGOSC_RUCHU 25
#define MAX_LICZBA_RUCHOW 25
#define WAGA 10000

#define BIALA_KROLOWKA 0
#define CZARNA_KROLOWKA 1
#define BIALY_PIONEK 2
#define CZARNY_PIONEK 3
#define PUSTE 4
#define KRAWEDZ 5

using namespace std;

enum KolorGracza { bialy = 0, czarny = 1 }; //kolor gracza albo bialy albo czarny

bool SprawdzCzyPionek(int kodPola)  //sprawdzanie czy na danym polu znajduje sie pionek
{
  if(kodPola < 4) return true;
  else return false;
}

bool CzyNalezyDoGracza(int pionek, KolorGracza gracz)
{
  if((pionek == 0 || pionek == 2) && (gracz == 0)) { return true; } //jesli pionek jest bialy lub czarny i zgadza sie to z kolorem
  else if((pionek == 1 || pionek == 3) && (gracz == 1)) { return true; } //gracza to zwraca true
  else { return false; }
}

//sprawdzanie koloru przeciwnika
KolorGracza Przeciwnik(KolorGracza gracz)
{
  if(gracz == czarny) return bialy;
  if(gracz == bialy) return czarny;
}

//Klasa przechowujaca liste mozliwych ruchow do wykonania
class ListaRuchow
{
public:
  int **lista;
  int liczbaRuchow;
  bool bicie;
  ListaRuchow();
};

ListaRuchow::ListaRuchow()
{
  lista = new int * [MAX_LICZBA_RUCHOW];
  for(int i = 0;i < MAX_LICZBA_RUCHOW; ++i)
    lista[i] = new int[DLUGOSC_RUCHU];
}

//////////////////////////////////////////////////////////////

//klasa opisujaca ruch pionka na planszy
class Ruch
{
public:
  int WartoscRuchu[6];
  int RuchPionka[64];
  int liczbaKrokow;
  int *plansza;
  KolorGracza gracz;
  ListaRuchow *lista_ruchow;

  Ruch();
  void ZapiszRuch();
  void StworzListe(int pionek, int poziom); //tworzenie listy ruchow
  void NowaLista(int *plansza, KolorGracza gracz, ListaRuchow *lista);
  void StworzPlansze(int *plansza); //tworzenie planszy
  void WykonajRuch(int Ruch[],int *plansza);
  bool BrakRuchow(int *plansza, KolorGracza gracz);
};


Ruch::Ruch()
{
  WartoscRuchu[0] = -4;
  WartoscRuchu[1] = -5;
  WartoscRuchu[2] = 5;
  WartoscRuchu[3] = 4;
  WartoscRuchu[4] = -4;
  WartoscRuchu[5] = -5;
  gracz = czarny;
}

//dodawanie ruchu do listy ruchow
void Ruch::ZapiszRuch()
{
  int *poczatek = RuchPionka;
  int *koniec = lista_ruchow->lista[lista_ruchow->liczbaRuchow++];
  poczatek[liczbaKrokow] = 0; // 0 - koniec ruchu
  do
    {
      *(koniec++) = *poczatek;
    } while(*(poczatek++));
}
//tworzy liste ruchow dla danego pionka
void Ruch::StworzListe(int pionek, int poziom)
{
  RuchPionka[liczbaKrokow++] = pionek;
  bool zapis = true;
  int i = (plansza[pionek] % 2) * 2; // 0 - biale 2 - czarne
  int koniec= i + (plansza[pionek] > CZARNA_KROLOWKA ? 2 : 4);

  for(; i < koniec; ++i)
    {
      int wr = WartoscRuchu[i];
      if(plansza[pionek + wr] == PUSTE && !lista_ruchow->bicie)
	{
	  if(liczbaKrokow == 1)
	    {
	      RuchPionka[liczbaKrokow++] = pionek + wr;
	      ZapiszRuch();
	      --liczbaKrokow;
	    }
	}
      else if(SprawdzCzyPionek(plansza[pionek + wr]) && CzyNalezyDoGracza(plansza[pionek + wr], Przeciwnik(gracz)) && plansza[pionek + 2 * wr] == PUSTE)
	{
	  if(!lista_ruchow->bicie)
	    {
	      lista_ruchow->bicie = true;
	      lista_ruchow->liczbaRuchow = 0;
	    }

	  RuchPionka[liczbaKrokow++] = pionek + wr;
	  plansza[pionek + 2 * wr] = plansza[pionek];
	  int temp = plansza[pionek+wr];
	  plansza[pionek + wr] = plansza[pionek] = PUSTE;
	  StworzListe(pionek + 2 * wr, poziom + 1);
	  plansza[pionek] = plansza[pionek + 2 * wr];
	  plansza[pionek + wr] = temp;
	  plansza[pionek + 2 * wr] = PUSTE;

	  liczbaKrokow--;
	  zapis= false;
	}
    }

  if(poziom && zapis)
    ZapiszRuch();
  --liczbaKrokow;
}

void Ruch::NowaLista(int *Plansza, KolorGracza g, ListaRuchow *lista)
{
  lista->liczbaRuchow = liczbaKrokow = 0;
  lista->bicie = false;
  plansza = Plansza;
  gracz = g;
  lista_ruchow = lista;

  for(int i = 10; i < 45; ++i)
    {
      if(SprawdzCzyPionek(Plansza[i]) && CzyNalezyDoGracza(Plansza[i], gracz))
	 StworzListe(i, 0);
    }
}

//tworzenie planszy
void Ruch::StworzPlansze(int *Plansza)
{
  for(int i = 0; i < 55; ++i) Plansza[i] = KRAWEDZ;
  for(int i = 10; i < 45; ++i) Plansza[i] = PUSTE;
  for(int i = 10; i < 23; ++i) Plansza[i] = CZARNY_PIONEK;
  for(int i = 32; i < 45; ++i) Plansza[i] = BIALY_PIONEK;

  Plansza[18] = Plansza[27] = Plansza[36] = KRAWEDZ;
}

void Ruch::WykonajRuch(int ruch[], int *Plansza)
{
  int pionek = Plansza[*ruch];
  int *ostatni;

  do
    {
      ostatni = ruch;
      Plansza[*(ruch++)] = PUSTE;
    } while(*ruch);

  if((*ostatni < 14 || *ostatni > 40) && pionek > CZARNA_KROLOWKA)
    pionek = pionek - 2;

  Plansza[*ostatni] = pionek;
}

bool Ruch::BrakRuchow(int *Plansza, KolorGracza gracz)
{
  for(int i = 0; i < 45; ++i)
    {
      if(SprawdzCzyPionek(Plansza[i]) && CzyNalezyDoGracza(Plansza[i], gracz))
	{
	  int j = (Plansza[i] % 2) * 2;
	  int koniec = j + ( Plansza[i] > CZARNA_KROLOWKA ? 2 : 4);

	  for(; j < koniec ; ++j)
	    {
	      int wr = WartoscRuchu[j];
	      if(Plansza[i + wr] == PUSTE )
		{
		  return false;
		}
	      if(SprawdzCzyPionek(Plansza[i + wr]) && CzyNalezyDoGracza(Plansza[i + wr], Przeciwnik(gracz))&& Plansza[i + 2 * wr] == PUSTE)
		{
		  return false;
		}
	    }
	}
    }
  return true;
}



Ruch Ruch;

#define PIONEK 100
#define KROLOWKA 130
#define WAGA_ZWYKLA 10

int ocena(int *Plansza, KolorGracza gracz)
{
  int wynik = 0;
  int BK = 0, CK = 0, BP = 0, CP = 0;

  for(int i = 10; i < 45; ++i)
    {
      if(SprawdzCzyPionek(Plansza[i]))
	{
	  int pionek = Plansza[i];

	  switch(pionek)
	    {
	    case BIALY_PIONEK: BP++; break;
	    case CZARNY_PIONEK: CP++; break;
	    case BIALA_KROLOWKA: BK++; break;
	    case CZARNA_KROLOWKA: CK++; break;
	    }
	}
    }

  if(Plansza[10] == BIALY_PIONEK && Plansza[12] == BIALY_PIONEK && CP > 1)
    wynik = wynik - WAGA_ZWYKLA;

  if(Plansza[44] == CZARNY_PIONEK && Plansza[42] == CZARNY_PIONEK && BP > 1)
    wynik = wynik + WAGA_ZWYKLA;

  int WAGA_CZARNY = CK * KROLOWKA + CP * PIONEK;
  int WAGA_BIALY= BK * KROLOWKA + BP * PIONEK;

  wynik = wynik + ((WAGA_CZARNY - WAGA_BIALY) * 200) / (WAGA_CZARNY + WAGA_BIALY);

  wynik = wynik + (WAGA_CZARNY - WAGA_BIALY);

  if(gracz == czarny) return wynik;
  else return -wynik;
}

struct RuchInfo{
  int ocena;
  int *ruch;
};

class RuchKomputera
{
public:
  ListaRuchow *lista;
  int **temp;
  int MaxPoziom;
  int poziomPrzeszukiwania;
  long liczba_ocen;
  long liczba_poziomow;

  RuchKomputera();
  void PoziomPrzeszukiwania(int poziom);
  int AlphaBeta(int *Plansza, int poziom, KolorGracza gracz, int min, int max, int p_poziom);
  RuchInfo AlphaBeta0(int *Plansza, int poziom, KolorGracza gracz);
  void Znajdz(int *Plansza, KolorGracza gracz, int *ruch);
  void WykonajRuch(int *Plansza, int *ruch, int *zapisz);
  void RuchWstecz(int *Plansza, int *ruch, int *zapisz);
  void KopiujRuch( int *koniec, int *poczatek);
  bool CzyJedyny(RuchInfo &info, int *Plansza, KolorGracza gracz);
  char* WyswietlRuch(int *ruch, char *napis);
  char * Konwersja(int ruch);
};

void RuchKomputera::PoziomPrzeszukiwania(int poziom)
{
  poziomPrzeszukiwania = poziom;
}

char * RuchKomputera::WyswietlRuch(int *ruch, char *napis)
{
  int pierwszy = *ruch;
  int i=0;

  while(*ruch)
    {
      ++ruch;
      ++i;
    }
  char temp[50];
  sprintf(temp, "%s%c%s", Konwersja(pierwszy), '-', Konwersja(*(ruch)));
  sprintf(napis, "%s%c%s", Konwersja(pierwszy), '-', Konwersja(*(--ruch)));
  return napis;
}
char tab[45][3];

char * RuchKomputera::Konwersja(int ruch)
{
  strcpy(tab[10],"B8");  strcpy(tab[11],"D8"); strcpy(tab[12],"F8");
  strcpy(tab[13],"H8");  strcpy(tab[14],"A7"); strcpy(tab[15],"C7");
  strcpy(tab[16],"E7");  strcpy(tab[17],"G7"); strcpy(tab[19],"B6");
  strcpy(tab[20],"D6");  strcpy(tab[21],"F6"); strcpy(tab[22],"H6");
  strcpy(tab[23],"A5");  strcpy(tab[24],"C5"); strcpy(tab[25],"E5");
  strcpy(tab[26],"G5");  strcpy(tab[28],"B4"); strcpy(tab[29],"D4");
  strcpy(tab[30],"F4");  strcpy(tab[31],"H4"); strcpy(tab[32],"A3");
  strcpy(tab[33],"C3");  strcpy(tab[34],"E3"); strcpy(tab[35],"G3");
  strcpy(tab[37],"B2");  strcpy(tab[38],"D2"); strcpy(tab[39],"F2");
  strcpy(tab[40],"H2");  strcpy(tab[41],"A1"); strcpy(tab[42],"C1");
  strcpy(tab[43],"E1");  strcpy(tab[44],"G1");

  return tab[ruch];
}

RuchKomputera::RuchKomputera()
{
  lista = new ListaRuchow[MAX_POZIOM];
  temp = new int*[MAX_POZIOM];

  for(int i = 0; i < MAX_POZIOM; ++i)
    temp[i] = new int[DLUGOSC_RUCHU];
}

void RuchKomputera::WykonajRuch(int *Plansza, int *ruch, int *zapisz)
{
  int pionek = Plansza[*ruch];

  do
    {
      *(zapisz++) = Plansza[*ruch];
      Plansza[*(ruch++)] = PUSTE;
    } while(*ruch);

  int OstatniePole = *(ruch-1);
  if((OstatniePole < 14|| OstatniePole > 40) && pionek > CZARNA_KROLOWKA)
    pionek = pionek - 2;
  Plansza[OstatniePole] = pionek;
}

void RuchKomputera::RuchWstecz(int *Plansza, int *ruch, int *zapisz)
{
  do
    {
      Plansza[*(ruch++)] = *(zapisz++);
    } while(*ruch);
}

void RuchKomputera::KopiujRuch(int *koniec, int *poczatek)
{
  do
    {
      *(koniec++) = *(poczatek++);
    } while(*poczatek);
  *koniec=0;
}

int RuchKomputera::AlphaBeta(int *Plansza, int poziom, KolorGracza gracz, int min, int max, int p_poziom)
{
  liczba_poziomow++;
  if(p_poziom > MaxPoziom) MaxPoziom = p_poziom;

  ListaRuchow &rlista = lista[p_poziom];
  Ruch.NowaLista(Plansza, gracz, &rlista);

  if(!rlista.liczbaRuchow)
    return -WAGA + p_poziom - 1;

  if(poziom < 1 && rlista.bicie)
    poziom = poziom + 1;

  if(poziom < 1 || p_poziom >= MAX_POZIOM - 1)
    {
      ++liczba_ocen;
      return ocena(Plansza, gracz);
    }

  int najlepszy = -WAGA;
  for(int i=0; i < rlista.liczbaRuchow && najlepszy < max; ++i)
    {
      WykonajRuch(Plansza, rlista.lista[i], temp[p_poziom]);
      int wynik = -AlphaBeta(Plansza, poziom - 1, Przeciwnik(gracz), -max, -min, p_poziom + 1);
      RuchWstecz(Plansza, rlista.lista[i], temp[p_poziom]);

      if(wynik > najlepszy)
	{
	  najlepszy = wynik;
	  if(wynik > min)
	    min = najlepszy;
	}
    }
  return najlepszy;
}
//algorytm minmax w po��czeniu z ci�ciami alpha-beta
RuchInfo RuchKomputera::AlphaBeta0(int *Plansza, int poziom, KolorGracza gracz)
{
  int min = -WAGA;
  int max = WAGA;

  ListaRuchow &rlista = lista[0];
  Ruch.NowaLista(Plansza, gracz, &rlista);

  int najlepszy = -WAGA;
  int najlepsza_pozcyja = 0;

  for(int i = 0; i < rlista.liczbaRuchow && najlepszy < max; ++i)
    {
      WykonajRuch(Plansza, rlista.lista[i], temp[0]);                                   //sprawdzamy oplacalnosc danego ruchu
      int wynik = -AlphaBeta(Plansza, poziom - 1, Przeciwnik(gracz), -max, -min, 1);    //wywolujac poprzednia metode.
      RuchWstecz(Plansza, rlista.lista[i], temp[0]);                                    //oceniajac ruch sprawdzamy czy jest on
                                                                                        //maksymalnie oplacalny i jesli tak, to
      if(wynik > najlepszy)                                                             //wybieramy wlasnie ten ruch
	{
	  najlepszy = wynik;
	  najlepsza_pozcyja = i;
	  if(wynik > min)
	    min = najlepszy;
	}
    }

  RuchInfo Temp;
  Temp.ocena= najlepszy;
  Temp.ruch= rlista.lista[najlepsza_pozcyja];
  return Temp;
}

bool RuchKomputera::CzyJedyny(RuchInfo &info, int *Plansza, KolorGracza gracz)
{
  ListaRuchow &rlista = lista[0];
  Ruch.NowaLista(Plansza, gracz, &rlista);

  if(rlista.liczbaRuchow != 1)
    return false;
  info.ruch= rlista.lista[0];
  return true;
}

void RuchKomputera::Znajdz(int *Plansza, KolorGracza gracz, int *ruch)
{
  RuchInfo ruchInfo;
  MaxPoziom = 0;
  liczba_ocen = 0;
  liczba_poziomow = 0;
  if(!CzyJedyny(ruchInfo, Plansza, gracz))
    {
      ruchInfo = AlphaBeta0(Plansza, poziomPrzeszukiwania, gracz);
    }
  KopiujRuch(ruch, ruchInfo.ruch);
}


RuchKomputera RuchKomputera;

char WyswietlPole(int kodPola)
{
  switch(kodPola){
  case BIALA_KROLOWKA: return 'B';
  case CZARNA_KROLOWKA: return 'C';
  case BIALY_PIONEK:  return 'b';
  case CZARNY_PIONEK:  return 'c';
  default:    return ' ';
  }
}

void WyswietlPlansze(int *Plansza){
  cout<<"   A   B   C   D   E   F   G   H"<<endl
      <<"  ___ ___ ___ ___ ___ ___ ___ ___"<<endl
      <<"8|   | "<<WyswietlPole(Plansza[10])<<" |   | "<<WyswietlPole(Plansza[11])<<" |   | "<<WyswietlPole(Plansza[12])<<" |   | "<<WyswietlPole(Plansza[13])<<" |8"<<endl
      <<" |___|___|___|___|___|___|___|___|"<<endl
      <<"7| "<<WyswietlPole(Plansza[14])<<" |   | "<<WyswietlPole(Plansza[15])<<" |   | "<<WyswietlPole(Plansza[16])<<" |   | "<<WyswietlPole(Plansza[17])<<" |   |7"<<endl
      <<" |___|___|___|___|___|___|___|___|"<<endl
      <<"6|   | "<<WyswietlPole(Plansza[19])<<" |   | "<<WyswietlPole(Plansza[20])<<" |   | "<<WyswietlPole(Plansza[21])<<" |   | "<<WyswietlPole(Plansza[22])<<" |6"<<endl
      <<" |___|___|___|___|___|___|___|___|"<<endl
      <<"5| "<<WyswietlPole(Plansza[23])<<" |   | "<<WyswietlPole(Plansza[24])<<" |   | "<<WyswietlPole(Plansza[25])<<" |   | "<<WyswietlPole(Plansza[26])<<" |   |5"<<endl
      <<" |___|___|___|___|___|___|___|___|"<<endl
      <<"4|   | "<<WyswietlPole(Plansza[28])<<" |   | "<<WyswietlPole(Plansza[29])<<" |   | "<<WyswietlPole(Plansza[30])<<" |   | "<<WyswietlPole(Plansza[31])<<" |4"<<endl
      <<" |___|___|___|___|___|___|___|___|"<<endl
      <<"3| "<<WyswietlPole(Plansza[32])<<" |   | "<<WyswietlPole(Plansza[33])<<" |   | "<<WyswietlPole(Plansza[34])<<" |   | "<<WyswietlPole(Plansza[35])<<" |   |3"<<endl
      <<" |___|___|___|___|___|___|___|___|"<<endl
      <<"2|   | "<<WyswietlPole(Plansza[37])<<" |   | "<<WyswietlPole(Plansza[38])<<" |   | "<<WyswietlPole(Plansza[39])<<" |   | "<<WyswietlPole(Plansza[40])<<" |2"<<endl
      <<" |___|___|___|___|___|___|___|___|"<<endl
      <<"1| "<<WyswietlPole(Plansza[41])<<" |   | "<<WyswietlPole(Plansza[42])<<" |   | "<<WyswietlPole(Plansza[43])<<" |   | "<<WyswietlPole(Plansza[44])<<" |   |1"<<endl
      <<" |___|___|___|___|___|___|___|___|"<<endl;
  cout<<"   A   B   C   D   E   F   G   H"<<endl;
}

void RuchGracza(int *Plansza, KolorGracza Player, int *ruch)
{
  ListaRuchow ruchlista;
  char napis[50];
  Ruch.NowaLista(Plansza,Player,&ruchlista);
  for(int i=0;i<ruchlista.liczbaRuchow;++i){
    RuchKomputera.WyswietlRuch(ruchlista.lista[i], napis);
    cout << (i+1) << ". " << napis << endl;
  }
  int wybor;
  do
    {
      printf("\nWybierz ruch [%d..%d]: ",1,ruchlista.liczbaRuchow);
      cin >> wybor;
    } while(wybor < 0 || wybor > ruchlista.liczbaRuchow);

  if(!wybor) { cout << "TCHORZ !" << endl; exit(0); }
  RuchKomputera.KopiujRuch(ruch, ruchlista.lista[wybor-1]);
}

enum TypGracza { GRACZ, Komputer };

class Warcaby
{
  int Plansza[55];
  KolorGracza ktoryGracz;
  char NazwaGracza[2][10];
  TypGracza rodzaj[2];
public:
  Warcaby();
  void start();
};

Warcaby::Warcaby()
{
  Ruch.StworzPlansze(Plansza);
  ktoryGracz = bialy;

  strcpy(NazwaGracza[bialy], "Bialy");
  strcpy(NazwaGracza[czarny], "Czarny");

  rodzaj[czarny]= Komputer;
  rodzaj[bialy]= GRACZ;
}

void Warcaby::start(){
  bool koniecGry= false;
  int ruch[DLUGOSC_RUCHU];

  while(!koniecGry)
    {
      system("ping -n 127.0.0.1 > NUL");
      system("cls");
      WyswietlPlansze(Plansza);

      if(rodzaj[ktoryGracz] == GRACZ)
	{
	  RuchGracza(Plansza, ktoryGracz, ruch);
	}
      else
	{
	  RuchKomputera.PoziomPrzeszukiwania(7);
	  RuchKomputera.Znajdz(Plansza, ktoryGracz, ruch);
	}

      Ruch.WykonajRuch(ruch, Plansza);
      if(Ruch.BrakRuchow(Plansza, Przeciwnik(ktoryGracz))){
	koniecGry = true;
      }
      else{
	ktoryGracz = Przeciwnik(ktoryGracz);
      }
    }
  cout << "Wygral gracz: " << NazwaGracza[ktoryGracz];
}


int main()
{
  Warcaby nowa_gra;
  nowa_gra.start();
  return 0;
}

